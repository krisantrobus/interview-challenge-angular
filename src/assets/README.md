# Angular Movies

## Running the App
This is a standard angular app with no extra dependencies to build angular libraries etc. just run ```npm i && ng serve```

## What's Included
- Services
    - movie-data.service.ts 
- Lazy Loading
- Inline Template, loading component
- Inputs & Outputs
- Directives
    - ngIf
    - ngFor
    - Custom directive - appCardHover *see below*
- Reusable Components & Directives
    - By having its own module and exporting the component to other modules
- View Child *see below*
- Observables & Subscribe
- Animations *see below*
- Typescript Class [model.ts](../app/modules/movie-module/model.ts)

## What's not Included
- Custom Angular library. I have previously done this, complete with including multiple dependencies to ship across applications.
- E2E testing

## Key Decisions
### Component Driven
As each film object has the same data structure a stateless card component has been created. This is reused on the movie home page to render each movie card. Integration is with the ```@input()``` and ```@Output``` decorator. Also utilising the ```ngIf``` directive

While the output captures and emits a click function it is just as easy to achieve this with the click of the ```<movie-movie-card>``` DOM. I have not done this to demonstrate the ability to output from components. 

### Bootstrap
Bootstrap was implemented as a simple way to style components and control the layout of the application. Examples include using the flex css features to correctly size and arrange content. It's also used to control the colours and theme the application. 

Some commonly used classes:
- col & row
- d-flex
- bg-${color}
- justify-content

The other use case was to implement a quick boilerplate navigation bar. I purposefully steered away from using the ngx-bootstrap components to demonstrate knowledge and capabilities of creating more complex angular interactions. e.g. custom modal.

## Movie Data Service
The data service uses the HttpClient provided by Angular. I return Observables back to component and subscribe to retrieve the values. 

I would usually on error use another service to provide feedback to the user but I've left that out of scope for this.
```
interview-challenge-angular\src\app\modules\movie-module\movies-data.service.ts

getRecentMovies(): Observable<any> {
    return this.http.get(
        `${this._apiUrl}${this._apiKey}&primary_release_date.gte=2019-05-01`,
        {
            headers: this._headers
        }
    );
}
```

```
interview-challenge-angular\src\app\modules\movie-module\pages\movie-home\movie-home.component.ts

ngOnInit() {
    this.mdataService.getRecentMovies().subscribe(
        data => {
            this._movies = <Movie[]>data.results;
        },
        err => {
            console.log(`err:`, err);
        }
    );
}
```

## Custom Directive - appCardHover
A custom directive had been created to allow for hover styling on the movie cards. It can be applied to any element to apply box shadow and cursor pointer stylings. It has its own module, exporting itself to make it available across the whole application.

The directive acheives this by using  host listener decorators to aply and remove styling using a highligh method
```
interview-challenge-angular\src\app\shared\directives\card-hover.directive.ts

@HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
}

@HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
}

// Highlightd based on whther the mose is over the element by applying box shadow styling
private highlight(active: boolean): void {
    this.el.nativeElement.style.boxShadow = active
        ? "2px 2px 12px rgba(60,67,111, 1)"
        : "none";

    this.el.nativeElement.style.cursor = active ? "pointer" : "none";
}
```

It's then added to the movie card compoent:
```
interview-challenge-angular\src\app\modules\movie-module\components\movie-card\movie-card.component.html

<div class="card m-2 p-0" appCardHover (click)="selectMovie()" *ngIf="this.movie"> ... </div>
```

## View Child
the ```@ViewChild``` decorator has been used to toggle the state of teh modal. On click in the parent component the toggleShow() method is called. This switches and active boolean which is used to determine the state.

First it is defined in the parent:
```
interview-challenge-angular\src\app\modules\movie-module\pages\movie-home\movie-home.component.html

...
    <movie-modal #movieModal [movie]="this.selectedMovie"></movie-modal>
...
```

Then I've defined the view child in the .ts file and called the method on a click function:
```
interview-challenge-angular\src\app\modules\movie-module\pages\movie-home\movie-home.component.ts

export class MovieHomeComponent implements OnInit {
  @ViewChild("movieModal") private movieModal: ModalComponent;

...

    selectMovie(movie: Movie) {
        this.selectedMovie = movie;
        this.movieModal.toggleShow();
    }
```

## Animations
Angular animations have been used on the modal. It is used to fade in and out by changing both the visibility and opacity style. This is achieved by defining the trigger on the element and using an advice boolean.

```
interview-challenge-angular\src\app\modules\movie-module\components\modal\modal.component.html

<div class="card container-fluid" id="modal-container" [@openClose]="this.active ? 'open' : 'closed'">
```

Next I've defined the animation in the ```@Component``` decorator:
```
@Component({
    selector: "movie-modal",
    templateUrl: "./modal.component.html",
    styleUrls: ["./modal.component.scss"],
    animations: [
    trigger('openClose', [
        state('open', style({
            visibility: 'visible',
            opacity: 1
        })),
        state('closed', style({
            visibility: 'hidden',
            opacity: 0
        })),
        transition('open => *', [
            animate(200)
        ]),
        transition('closed => open', [
            animate(200)
        ]),
    ])
]
})
```

## Plugin -ngx-markdown
This plugin was included to both demonstrate the ability to import and use other libraries and to add another route into the app, demonstrating lazy-loading

## Testing
**Links only work in IDE not in browser**

Some basic unit testing has been demonstrated in both the components:
### [MovieCardComponent](../app/modules/movie-module/components/movie-card/movie-card.component.spec.ts)
Simple test to pass in a dummy input object and check that the title has rendered correctly
```
interview-challenge-angular\src\app\modules\movie-module\components\movie-card\movie-card.component.spec.ts

it('should render movie', () => {
    component.movie = DUMMY_MOVIE;
    fixture.detectChanges();
    const movieCard = fixture.debugElement.nativeElement;
    console.log(movieCard);
    expect(movieCard.querySelector('p').textContent).toContain(DUMMY_MOVIE.title);
});
```
### [ModalComponent](../app/modules/movie-module/components/modal/modal.component.spec.ts)
Runs the toggle method and checks that the component is hidden
```
interview-challenge-angular\src\app\modules\movie-module\components\modal\modal.component.spec.ts

it('should toggle active', () => {
    component.active = true;
    fixture.detectChanges();
    component.toggleShow()
    expect(component.active).toEqual(false);
});
```

Positive and negative cases for checking the state (style.visibility) of a component
```
interview-challenge-angular\src\app\modules\movie-module\components\modal\modal.component.spec.ts

it('should hide modal', () => {
    component.movie = DUMMY_MOVIE;
    fixture.detectChanges();
    const modalCompiled = fixture.debugElement.nativeElement;
    expect(modalCompiled.querySelector("#modal-container").style.visibility).toBe('hidden');
});

it('should show modal', () => {
    component.movie = DUMMY_MOVIE;
    component.active = true;
    fixture.detectChanges();
    const modalCompiled = fixture.debugElement.nativeElement;
    expect(modalCompiled.querySelector("#modal-container").style.visibility).toBe('visible');
});
```
## Disclaimer
Due to css flex usage the app is incompatible with < IE11. It is however compatible with Edge.