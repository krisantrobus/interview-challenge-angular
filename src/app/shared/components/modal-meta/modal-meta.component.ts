import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'movie-modal-meta',
  templateUrl: './modal-meta.component.html',
  styleUrls: ['./modal-meta.component.scss']
})
export class ModalMetaComponent implements OnInit {
  @Input() public class: string = "text-success fa-star";
  @Input() public text: string = null;

  constructor() { }

  ngOnInit() {
  }

}
