import { Component, OnInit } from '@angular/core';

// Using inline templates as the component is very small
@Component({
  selector: 'movie-loading',
  template: `
  <div class="container-fluid h-100">
      <div class="spinner-grow" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>`,
})
export class LoadingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
