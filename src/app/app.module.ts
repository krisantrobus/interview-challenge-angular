import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./shared/components/header/header.component";
import { CardDirectiveModule } from "./shared/directives/card-directive.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalMetaComponent } from './shared/components/modal-meta/modal-meta.component';


@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [BrowserModule, AppRoutingModule, CardDirectiveModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
