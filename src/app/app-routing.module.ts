import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/movies", pathMatch: "full" },
  {
    path: "movies",
    loadChildren:
      "./modules/movie-module/movie-routing.module#MovieRoutingModule"
  },
  {
    path: "info",
    loadChildren:
      "./modules/info-module/info-routing.module#InfoRoutingModule"

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }