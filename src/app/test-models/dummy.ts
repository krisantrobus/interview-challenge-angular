import { Movie } from '../modules/movie-module/model';
// Dummy data for testing
export const DUMMY_MOVIE: Movie = new Movie(11, 7, 'kung-fu dummy', '2019/07/04', 'Dummy movie for testing');