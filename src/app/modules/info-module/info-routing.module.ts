import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { InfoComponent } from './pages/info/info.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MarkdownModule } from 'ngx-markdown';


const routes: Routes = [
  { path: "", redirectTo: "/info/main", pathMatch: "full" },
  { path: "main", component: InfoComponent }
];

@NgModule({
  declarations: [InfoComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
  ],
  exports: [RouterModule]
})
export class InfoRoutingModule { }
