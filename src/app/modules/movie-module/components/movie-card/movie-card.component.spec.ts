import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieCardComponent } from './movie-card.component';
import { DUMMY_MOVIE } from '../../../../test-models/dummy';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render movie', () => {
    component.movie = DUMMY_MOVIE;
    fixture.detectChanges();
    const movieCard = fixture.debugElement.nativeElement;
    expect(movieCard.querySelector('p').textContent).toContain(DUMMY_MOVIE.title);
  });
});
