import { Component, OnInit, Input, HostListener, ElementRef } from "@angular/core";
import { Movie } from "../../model";
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: "movie-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"],
  animations: [
    trigger('openClose', [
      state('open', style({
        visibility: 'visible',
        opacity: 1
      })),
      state('closed', style({
        visibility: 'hidden',
        opacity: 0
      })),
      transition('open => *', [
        animate(200)
      ]),
      transition('closed => open', [
        animate(200)
      ]),
    ])
  ]
})
export class ModalComponent implements OnInit {
  @Input() public movie: Movie = null;
  constructor(private el: ElementRef) { }

  public active: boolean = false;

  ngOnInit() { }

  // Toggle showing of card based on the active variable
  public toggleShow() {
    this.active = !this.active;
  }

  // This was to hide the modal on click out of element when active. 
  // Didn't get working (registers clicks outside correctly) but toggle active shows 
  // first then immediatly collapses
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.el.nativeElement.contains(event.target) && this.active) {
      // this.active = false
    }

  }
}
