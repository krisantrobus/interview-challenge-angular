import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";


// A serice to contact the moviedb API
@Injectable()
export class MoviesDataService {
  constructor(private http: HttpClient) { }

  // Vairables to store tockens.
  // Normally I'd store these as cookies for accessing on authentication
  _apiKey: string = "?api_key=a6a7225bf6cb0f8135977c3c900fa0eb";
  _apiTokenL: string =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhNmE3MjI1YmY2Y2IwZjgxMzU5NzdjM2M5MDBmYTBlYiIsInN1YiI6IjVkM2YyNGM2MzRlMTUyMzhmY2U4ZjJkZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.4Ru1asGm7vnXaXv2waJE2Ff0RvmIqkMwz3YLqvTmNKs";

  _headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  // Store the api url so it can be used by many methods
  _apiUrl: string = "https://api.themoviedb.org/4/discover/movie";

  // use the HttpsClient to call the api and return an Observable
  getRecentMovies(): Observable<any> {
    return this.http.get(
      `${this._apiUrl}${this._apiKey}&primary_release_date.gte=2019-05-01`,
      {
        headers: this._headers
      }
    );
  }
}
