import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MoviesDataService } from "./movies-data.service";
import { HttpClientModule } from "@angular/common/http";
import { MovieHomeComponent } from "./pages/movie-home/movie-home.component";
import { MovieCardComponent } from "./components/movie-card/movie-card.component";
import { CommonModule } from "@angular/common";
import { CardDirectiveModule } from "src/app/shared/directives/card-directive.module";
import { ModalComponent } from "./components/modal/modal.component";
import { LoadingModule } from '../../shared/components/loading/loading.module';
import { ModalMetaModule } from '../../shared/components/modal-meta/modal-meta.module';


const routes: Routes = [
  { path: "", redirectTo: "/movies/recent", pathMatch: "full" },
  { path: "recent", component: MovieHomeComponent }
];

@NgModule({
  declarations: [MovieHomeComponent, MovieCardComponent, ModalComponent],
  providers: [MoviesDataService, HttpClientModule],
  imports: [
    RouterModule.forChild(routes),
    HttpClientModule,
    CommonModule,
    CardDirectiveModule,
    LoadingModule,
    ModalMetaModule
  ],
  exports: [RouterModule]
})
export class MovieRoutingModule { }
