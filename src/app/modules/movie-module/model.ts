export class Movie {
  adult: boolean;
  backdrop_path: String;
  genre_ids: number[];
  id: number;
  original_language: String;
  original_title: String;
  overview: String;
  popularity: number;
  poster_path: String;
  release_date: String;
  title: String;
  video: boolean;
  vote_average: number;
  vote_count: number;

  constructor(vote_count: number = null, vote_average: number = null, title: string = null, release_date: string = null, overview: string = null, video: boolean = false, poster_path: string = null, popularity: number = null, original_title: string = null, original_language: string = 'English', id: number = null, genre_ids: number[] = null, backdrop_path: string = null, adult: boolean = false) {
    this.adult = adult;
    this.backdrop_path = backdrop_path;
    this.genre_ids = genre_ids;
    this.id = id;
    this.original_language = original_language;
    this.original_title = original_title;
    this.overview = overview;
    this.popularity = popularity;
    this.poster_path = poster_path;
    this.release_date = release_date;
    this.title = title;
    this.video = video;
    this.vote_average = vote_average;
    this.vote_count = vote_count;
  }
}
