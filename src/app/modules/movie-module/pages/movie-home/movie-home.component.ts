import { Component, OnInit, ViewChild } from "@angular/core";
import { MoviesDataService } from "../../movies-data.service";
import { Movie } from "../../model";
import { ModalComponent } from "../../components/modal/modal.component";

@Component({
  selector: "movie-movie-home",
  templateUrl: "./movie-home.component.html",
  styleUrls: ["./movie-home.component.scss"]
})
export class MovieHomeComponent implements OnInit {
  @ViewChild("movieModal") private movieModal: ModalComponent;
  constructor(private mdataService: MoviesDataService) { }

  _movies: Movie[] = null;
  selectedMovie: Movie = null;

  // Populate the _movies variable when this component first loads to pass into child components
  ngOnInit() {
    this.mdataService.getRecentMovies().subscribe(
      data => {
        this._movies = <Movie[]>data.results;
      },
      err => {
        console.log(`err:`, err);
      }
    );
  }

  // Function to select the movie on click and open the custom modal
  selectMovie(movie: Movie) {
    this.selectedMovie = movie;
    this.movieModal.toggleShow();
  }
}
